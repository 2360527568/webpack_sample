// import $ from 'jquery'
// import $ from 'expose-loader?$!jquery'
// console.log($)
// console.log(window.$)

import Vue from 'vue'

import App from './app.vue'
import './index.less'

let image = new Image()
image.src = require('./shareqq.png')
document.body.appendChild(image)

new Vue({
    render: h => h(App)
}).$mount('#app')

// let vm = new Vue({
//     el: '#app',
//     components: {
//         test
//     }
// })
// require('./index.css')
// require('./index.less')
// console.log('成功引入index.js')
// let fn = () => {
//     console.log('箭头函数')
// }
// fn()

// // 装饰器
// @log
// class A {
//     a = 1
// }

// let a = new A()
// console.log('实例化对象:', a)

// // 装饰器函数
// function log(target) {
//     console.log('类:', target)
// }
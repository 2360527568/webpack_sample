const path = require('path')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractCss = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const UglifyjsWebpackPlugin = require('uglifyjs-webpack-plugin')
const webpack = require('webpack')

module.exports = {
    optimization: { // 优化项
        minimizer: [
            new UglifyjsWebpackPlugin({
                cache: true,
                parallel: true,
                sourceMap: true
            }),
            new OptimizeCssAssetsWebpackPlugin()
        ]
    },
    mode: 'development',
    entry: './src/index.js',
    output: {
        filename: 'bundle.[hash:8].js',
        path: path.resolve(__dirname, 'build'),
        // cdn地址 (全局)
        // publicPath: 'http://cdn.example.com'
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html',
        }),
        new MiniCssExtractCss({
            filename: 'css/main.css'
        }),
        // new webpack.ProvidePlugin({ // 在每个模块中注入 $对象
        //     $: 'jquery'
        // })
    ],
    externals: {
        jquery: '$'
    },
    module: {
        rules: [{
                test: /\.vue$/,
                use: ['vue-loader']
            },
            {
                test: /\.html$/,
                use: 'html-withimg-loader'
            },
            {
                test: /\.(png|jpg|gif)$/,
                // 当图片小于多少k的时候转化为 base64
                // 否则用 file-loader 直接产出
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 1 * 1024,
                        // cdn地址 (局部)
                        publicPath: 'http://cdn.example.com',
                        outputPath: '/img/'
                    }
                }
            },
            // {
            //     test: /\.(png|jpg|gif)$/,
            //     use: 'file-loader'
            // },
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: { // 用babel-loader 需要把es6-es5
                        presets: [
                            '@babel/preset-env'
                        ],
                        plugins: [
                            ['@babel/plugin-proposal-decorators', {
                                'legacy': true
                            }],
                            ['@babel/plugin-proposal-class-properties', {
                                'loose': true
                            }]
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractCss.loader, 'css-loader', 'postcss-loader']
            },
            {
                test: /\.less$/,
                use: [MiniCssExtractCss.loader, 'css-loader', 'postcss-loader', 'less-loader']
            }
        ]
    }
}
// webpack是基于node开发的 所以配置文件是支持node的
let path = require('path')
let HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    devServer: { // 开发服务器的配置
        port: 3000,
        progress: true,
        contentBase: './build'
    },
    mode: 'production', // 模式 默认两种 production 和 development
    entry: './src/index.js', // 入口文件
    output: {
        filename: 'bundle.[hash:8].js', // 打包后的输出文件名 hash八位
        path: path.resolve(__dirname, 'build') // 输出路径必须是一个绝对路径
    },
    plugins: [ // 数组 放着所有的webpack插件配置
        new HtmlWebpackPlugin({
            template: './src/index.html',
            filename: 'index.html',
            minify: { // html 压缩配置
                removeAttributeQuotes: true,
                collapseWhitespace: true,
            },
            hash: true
        })
    ]
}